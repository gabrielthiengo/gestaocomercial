<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cliente;
use App\Endereco;
use App\Empresa;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();

        return view('cliente-list', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresas = Empresa::all();
        return view('cliente-new', compact('empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = [
            'required' => 'Campo Obrigatório',
            'integer' => 'Insira um valor válido',
        ];

        $request->validate([
            'nome' => 'required',
            'email' => 'required',
            'cpf' => 'required',
            'rg' => 'required',
            'logradouro' => 'required',
            'bairro' => 'required',
            'municipio' => 'required',
            'numero' => 'required',
            'cep' => 'required',
        ], $message);

        $cliente = new Cliente();
        $endereco = new Endereco();

        $endereco->logradouro = $request->input('logradouro');
        $endereco->bairro = $request->input('bairro');
        $endereco->numero = $request->input('numero');
        $endereco->municipio = $request->input('municipio');
        $endereco->cep = $request->input('cep');
        $endereco->ponto_referencia = $request->input('referencia');
        $endereco->save();

        $cliente->nome = $request->input('nome');
        $cliente->telefone_fixo = $request->input('telfixo');
        $cliente->telefone_celular = $request->input('telcel');
        $cliente->email = $request->input('email');
        $cliente->rg = $request->input('rg');
        $cliente->cpf = $request->input('cpf');
        $cliente->empresa_id = $request->input('escolhaEmpresa');
        $cliente->endereco_id = $endereco->id;
        $cliente->save();

        return redirect('/clientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);

        if(isset($cliente)){
            $cliente->delete();
        }

        return redirect('/clientes');
    }
}
