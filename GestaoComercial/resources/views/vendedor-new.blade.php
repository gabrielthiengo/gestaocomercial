@extends('layouts.app')

@section('body')

<div class="content">
    <div class="jumbotron border border-secondary">
        <div class="card-body">
            <h5 class="card-title mb-4">Cadastrar Vendedor</h5>
            <hr>
            <br>
            <form action="/vendedor-create" method="POST">
                @csrf
                <div class="row">
                    <div class="input-group mb-3 col-4">
                        <div class="input-group-prepend">
                          <label class="input-group-text" for="inputGroupSelect01">Loja</label>
                        </div>
                        <select class="custom-select {{ $errors->has('escolhaLoja') ? 'is-invalid' : '' }}" name="escolhaLoja" id="inputGroupSelect01">
                            <option selected> Escolha... </option>
                            @foreach ($lojas as $loja)
                                <option value="{{$loja->id}}"> {{$loja->nome}} </option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            @if ($errors->has('escolhaLoja'))
                                {{ $errors->first('escolhaLoja') }}
                            @endif
                        </div>
                      </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="nome">Nome:</label> <label class="required" >*</label>
                            <input type="text" id="nome" autocomplete="off" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome" placeholder="Ex: Joaquim Teixeira Santos">
                            <div class="invalid-feedback">
                                @if ($errors->has('nome'))
                                    {{ $errors->first('nome') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="telfixo">Telefone Fixo:</label>
                            <input type="text" id="telfixo" autocomplete="off" class="form-control" name="telfixo" placeholder="Ex: (31) 9999-9999">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="telcel">Telefone Celular:</label>
                            <input type="text" id="telcel" autocomplete="off" class="form-control" name="telcel" placeholder="Ex: (31) 9 9999-9999">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="telcel">Email:</label> <label class="required" >*</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" name="email" autocomplete="off" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Ex: email@email.com" aria-label="Username" aria-describedby="basic-addon1">
                                <div class="invalid-feedback">
                                    @if ($errors->has('email'))
                                        {{ $errors->first('email') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <label for="dtnasc">Dt. Nascimento:</label> <label class="required" >*</label>
                            <input type="text" id="dtnasc" autocomplete="off" class="form-control {{ $errors->has('dtnasc') ? 'is-invalid' : '' }}" name="telfixo" placeholder="Ex: 21/03/1990">
                            <div class="invalid-feedback">
                                @if ($errors->has('dtnasc'))
                                    {{ $errors->first('dtnasc') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="cpf">CPF:</label> <label class="required" >*</label>
                            <input type="text" id="cpf" autocomplete="off" class="form-control {{ $errors->has('cpf') ? 'is-invalid' : '' }}" name="cpf" placeholder="Ex: 111.111.111-11">
                            <div class="invalid-feedback">
                                @if ($errors->has('cpf'))
                                    {{ $errors->first('cpf') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="rg">RG:</label> <label class="required" >*</label>
                            <input type="text" id="rg" autocomplete="off" class="form-control {{ $errors->has('rg') ? 'is-invalid' : '' }}" name="rg" placeholder="Ex: SP 00.000.000">
                            <div class="invalid-feedback">
                                @if ($errors->has('rg'))
                                    {{ $errors->first('rg') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="logradouro">Logradouro:</label> <label class="required" >*</label>
                            <input type="text" id="logradouro" autocomplete="off" class="form-control {{ $errors->has('logradouro') ? 'is-invalid' : '' }}" name="logradouro" placeholder="Ex: Rua X">
                            <div class="invalid-feedback">
                                @if ($errors->has('logradouro'))
                                    {{ $errors->first('logradouro') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="numero">Número:</label> <label class="required" >*</label>
                            <input type="number" id="numero" autocomplete="off" class="form-control {{ $errors->has('numero') ? 'is-invalid' : '' }}" name="numero" placeholder="Ex: 458">
                            <div class="invalid-feedback">
                                @if ($errors->has('numero'))
                                    {{ $errors->first('numero') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="cep">CEP:</label> <label class="required" >*</label>
                            <input type="text" id="cep" autocomplete="off" class="form-control {{ $errors->has('cep') ? 'is-invalid' : '' }}" name="cep" placeholder="Ex: 111.111.111">
                            <div class="invalid-feedback">
                                @if ($errors->has('cep'))
                                    {{ $errors->first('cep') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="referencia">Referência:</label>
                            <input type="text" id="referencia" autocomplete="off" class="form-control" name="referencia" placeholder="Ex: próx rua y">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="bairro">Bairro:</label> <label class="required" >*</label>
                            <input type="text" id="bairro" class="form-control {{ $errors->has('bairro') ? 'is-invalid' : '' }}" name="bairro" placeholder="Ex: Copacabana">
                            <div class="invalid-feedback">
                                @if ($errors->has('bairro'))
                                    {{ $errors->first('bairro') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="municipio">Município:</label> <label class="required" >*</label>
                            <input type="text" id="municipio" class="form-control {{ $errors->has('municipio') ? 'is-invalid' : '' }}" name="municipio" placeholder="Ex: Rio de Janeiro">
                            <div class="invalid-feedback">
                                @if ($errors->has('municipio'))
                                    {{ $errors->first('municipio') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <label class="my-1 mr-2" for="escolhaEmpresa"> Empresa </label>
                <select class="custom-select my-1 mr-sm-2" name="escolhaEmpresa" id="escolhaEmpresa">
                    <option selected> Escolha... </option>
@foreach ($empresas as $empresa)
                    <option value="{{$empresa->id}}"> {{$empresa->nome_fantasia}} </option>
@endforeach
                    </select>
                    </div>
                </div>

                <div class="pr-3" style="text-align: right;">
                    <button type="button" onClick="cancelar()" class="btn btn-cancel">Cancelar</button>
                    <button type="submit" class="btn btn-default ml-2">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('javascript')

<script type="text/javascript">

    //máscaras
    $('#cpf').mask('000.000.000-00', {reverse: true});
    $('#telfixo').mask('(00) 0000-0000', {reverse: false});
    $('#telcel').mask('(00) 9 0000-0000', {reverse: false});
    $('#rg').mask('AA 00.000.000', {reverse: false});
    $('#dtnasc').mask('00/00/0000', {reverse: true});
    $('#cep').mask('00.000-000', {reverse: true});

    $( function() {
    $( "#dtnasc" ).datepicker({
        datepicker: true,
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        default: '01/01/2020',
        weeks: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        showAnim: 'drop',
    });
  });

  function cancelar() {
        var url = '{{ route("lista_vendedores") }}';
        window.location.href = url;
    }

</script>

@endsection
