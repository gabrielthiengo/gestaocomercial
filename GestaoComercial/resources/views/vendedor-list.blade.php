@extends('layouts.app')

@section('body')

<div class="content">
        <div class="card border mt-3 table-style">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="card-title"><i class="fas fa-user-alt"></i>   Lista de Vendedores</h5>
                    </div>
                    <div class="col-2" style="text-align: right;">
                        <a class="btn btn-create" href="{{ route('novo_vendedor') }}"><i class="fas fa-plus"></i><strong>   Vendedor</strong></a>
                    </div>
                </div>
                <table class="table table-ordered table-hover table-striped" id="table_vendedor">
                    <thead>
                        <tr>
                            <th>Id <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Email <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Celular <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Data de Nascimento <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Deletar <i class="fas fa-sort-alpha-down ml-2"></i></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($vendedores as $vend)
                            <tr>
                                <td>{{$vend->id}}</td>
                                <td>{{$vend->nome}}</td>
                                <td>{{$vend->email}}</td>
                                <td>{{$vend->telefone_celular}}</td>
                                <td>{{$vend->dt_nascimento}}</td>
                                <td> <a href="/vendedor/deletar/{{ $vend->id }}" class="btn btn-sm btn-danger">Deletar</a> </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
</div>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {
    $('#table_vendedor').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhum vendedor",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
