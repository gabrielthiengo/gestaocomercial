<div class="sidebar" id="sidebar">

    <div class="loja-info">
        <div class="foto">FOTO</div>
        <div class="loja-nome">
            Flô Vestuário Contagem
        </div>
    </div>

    <table class="table table-ordered table-sidebar">
        <tbody>
            <tr>
                <td class="td-sidebar"><a href="{{ route('lista_produtos') }}"><i class="fas fa-archive"></i>   Produtos</a></td>
            </tr>
            <tr>
                <td class="td-sidebar"><a href="{{ route('lista_estoque') }}" ><i class="fas fa-folder-open"></i>   Estoque</a></td>
            </tr>
            <tr>
                <td class="td-sidebar"><a href="{{ route('lista_clientes') }}"><i class="fas fa-user-circle"></i>   Clientes</a></td>
            </tr>
            <tr>
                <td class="td-sidebar"><a href="{{ route('lista_vendedores') }}"><i class="fas fa-user-alt"></i>   Vendedores</a></td>
            </tr>
            <tr>
                <td class="colaps">
                <button class="collapsible"><i class="fas fa-pencil-alt"></i>   Cadastro</button>
                    <div class="colapse">
                        <div class="colapse-item">
                               <a href="{{ route('colecoes_list') }}"><strong style="color: #ff8c00;"> - </strong>Coleções</a>
                        </div>
                        <div class="colapse-item">
                            <a href="{{ route('grupos_list') }}"><strong style="color: #ff8c00;"> - </strong>Fornecedor</a>
                        </div>
                        <div class="colapse-item">
                            <a href="{{ route('grades-list') }}"><strong style="color: #ff8c00;"> - </strong>Grades</a>
                        </div>
                        <div class="colapse-item">
                            <a href="{{ route('grupos_list') }}"><strong style="color: #ff8c00;"> - </strong>Grupos</a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="td-sidebar"><a href="#"><i class="fas fa-users-cog"></i>   Configurações</a></td>
            </tr>

        </tbody>
    </table>
</div>


@section('javascript1')

<script type="text/javascript">

    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
            content.style.maxHeight = null;
            } else {
            content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }

</script>

@endsection
