@extends('layouts.app')

@section('body')

<div class="content">
    <div class="jumbotron border border-secondary">
        <div class="card-body">
            <h5 class="card-title mb-4">Cadastrar Produtos</h5>
            <hr>
            <br>
            <form action="/produtos" method="POST" id="formProduto">
                @csrf

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="codigo">Codigo:</label> <label class="required" >*</label>
                            <input type="text" id="codigo" class="form-control {{ $errors->has('codigo') ? 'is-invalid' : '' }}" name="codigo" placeholder="Ex: VR001202090">
                            <div class="invalid-feedback">
                                @if ($errors->has('codigo'))
                                    {{ $errors->first('codigo') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group">
                            <label for="nome">Nome:</label> <label class="required" >*</label>
                            <input type="text" id="nome" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome" placeholder="Ex: Regata">
                            <div class="invalid-feedback">
                                @if ($errors->has('nome'))
                                    {{ $errors->first('nome') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="descricao"> Descrição </label>
                            <textarea class="form-control" id="descricao" rows="3" name="descricao"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="custo">Custo:</label> <label class="required" >*</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">$</span>
                                </div>
                                <input type="text" id="custo" class="form-control {{ $errors->has('custo') ? 'is-invalid' : '' }}"
                                aria-label="Amount (to the nearest dollar)" name="custo" placeholder="99,99">
                                <div class="invalid-feedback">
                                    @if ($errors->has('custo'))
                                        {{ $errors->first('custo') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="unitario">Valor Unitário:</label> <label class="required" >*</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">$</span>
                                </div>
                                <input type="text" id="unitario" class="form-control {{ $errors->has('unitario') ? 'is-invalid' : '' }}"
                                aria-label="Amount (to the nearest dollar)" name="unitario" placeholder="99,99">
                                <div class="invalid-feedback">
                                    @if ($errors->has('unitario'))
                                        {{ $errors->first('unitario') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="revenda">Valor Revenda:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">$</span>
                                </div>
                                <input type="text" id="revenda" class="form-control {{ $errors->has('revenda') ? 'is-invalid' : '' }}"
                                aria-label="Amount (to the nearest dollar)" name="revenda" placeholder="99,99">
                                <div class="invalid-feedback">
                                    @if ($errors->has('revenda'))
                                        {{ $errors->first('revenda') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="porcentLucro">Porcentagem de Lucro:</label>
                            <input type="text" id="porcentLucro" class="form-control {{ $errors->has('porcentLucro') ? 'is-invalid' : '' }}" name="porcentLucro" placeholder="Ex: 65%">
                            <div class="invalid-feedback">
                                @if ($errors->has('porcentLucro'))
                                    {{ $errors->first('porcentLucro') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="icms">ICMS:</label>
                            <input type="text" id="icms" class="form-control {{ $errors->has('icms') ? 'is-invalid' : '' }}" name="icms" placeholder="Ex: 65%">
                            <div class="invalid-feedback">
                                @if ($errors->has('icms'))
                                    {{ $errors->first('icms') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="pis">PIS:</label>
                            <input type="text" id="pis" class="form-control {{ $errors->has('pis') ? 'is-invalid' : '' }}" name="pis" placeholder="Ex: 65%">
                            <div class="invalid-feedback">
                                @if ($errors->has('pis'))
                                    {{ $errors->first('pis') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="cofins">Cofins:</label>
                            <input type="text" id="cofins" class="form-control {{ $errors->has('cofins') ? 'is-invalid' : '' }}" name="cofins" placeholder="Ex: 65%">
                            <div class="invalid-feedback">
                                @if ($errors->has('cofins'))
                                    {{ $errors->first('cofins') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="ipi">IPI:</label>
                            <input type="text" id="ipi" class="form-control {{ $errors->has('ipi') ? 'is-invalid' : '' }}" name="ipi" placeholder="Ex: 65%">
                            <div class="invalid-feedback">
                                @if ($errors->has('ipi'))
                                    {{ $errors->first('ipi') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <label class="my-1 mr-2" for="escolhaGrupos"> Grupo </label>
                        <select class="custom-select my-1 mr-sm-2" name="escolhaGrupos" id="escolhaGrupos">
                            <option selected> Escolha... </option>
@foreach ($grupos as $grupo)
                            <option value="{{$grupo->id}}"> {{$grupo->nome}} </option>
@endforeach
                        </select>
                    </div>
                    <div class="col-4">
                        <label class="my-1 mr-2" for="escolhaColecao"> Coleção </label>
                        <select class="custom-select my-1 mr-sm-2" name="escolhaColecao" id="escolhaColecao">
                            <option selected> Escolha... </option>
@foreach ($colecoes as $colecao)
                            <option value="{{$colecao->id}}"> {{$colecao->nome}} </option>
@endforeach
                        </select>
                    </div>
                    <div class="col-4">
                        <label class="my-1 mr-2" for="escolhaGrade"> Grade </label>
                        <select class="custom-select my-1 mr-sm-2" name="escolhaGrade" id="escolhaGrade">
                            <option selected> Escolha... </option>
@foreach ($grades as $grade)
                            <option value="{{$grade->id}}"> {{$grade->nome}} </option>
@endforeach
                        </select>
                    </div>
                </div>

                <hr>

                <div class="pr-3" style="text-align: right;">
                    <button type="button" onClick="cancelar()" class="btn btn-cancel">Cancelar</button>
                    <button type="button" onClick="validateForm()" class="btn btn-default ml-2">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection


@section('sweetalert')

<script type="text/javascript">

function validateForm() {
    event.preventDefault(); // prevent form submit
    swal({
		title: "Cadastrar Produto?",
		text: "Deseja cadastrar o produto informado?",
		type: "success"
		showCancelButton: true,
		confirmButtonColor: '#82c15f',
		confirmButtonText: "Sim",
		cancelButtonText: "Não",
		closeOnConfirm: false,
		closeOnCancel: true
	},
	function(isConfirm){
		if (isConfirm){
            $("#formProduto").submit();
		} else {

		}
	});
}
    function cancelar() {
        var url = '{{ route("lista_produtos") }}';
        window.location.href = url;
    }

</script>

@endsection
