@extends('layouts.app')

@section('body')

<div class="content">
    <div class="jumbotron bg-light border border-secondary">
        <div class="card-body">
            <h5 class="card-title mb-4">Cadastrar Coleção</h5>
            <hr>
            <br>
            <form action="/colecoes" method="POST">
                @csrf

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="codigo"> Código :</label>
                            <input type="text" id="codigo" class="form-control {{ $errors->has('codigo') ? 'is-invalid' : '' }}" name="codigo"
                            readonly="readonly" value="Disponível após a conclusão">
                            <div class="invalid-feedback">
                                @if ($errors->has('codigo'))
                                    {{ $errors->first('codigo') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group">
                            <label for="nome">Nome:</label>
                            <input type="text" id="nome" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome">
                            <div class="invalid-feedback">
                                @if ($errors->has('nome'))
                                    {{ $errors->first('nome') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="descricao"> Descrição </label>
                            <textarea class="form-control" id="descricao" rows="3" name="descricao"></textarea>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="pr-3" style="text-align: right;">
                    <button type="button" onClick="cancelar()" class="btn btn-cancel">Cancelar</button>
                    <button type="submit" class="btn btn-default ml-2">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('javascript')

<script type="text/javascript">

  function cancelar() {
        var url = '{{ route("colecoes_list") }}';
        window.location.href = url;
    }

</script>

@endsection
