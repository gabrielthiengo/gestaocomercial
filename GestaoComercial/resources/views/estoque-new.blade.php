@extends('layouts.app')

@section('body')

<div class="content">
    <div class="jumbotron border border-secondary">
        <div class="card-body">
            <h5 class="card-title mb-4"><i class="fas fa-plus"></i>   Adicionar Produto</h5>
            <hr>
            <br>
            <form action="/estoque" method="POST">
                @csrf

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="codigo">Codigo:</label> <label class="required" >*</label>
                            <div class="tooltip"><i class="fas fa-question"></i>
                                <span class="tooltiptext">Tecle enter após inserir o código para buscar.</span>
                            </div>
                            @if ($buscar == true)
                            <input value="{{ $produto->codigo }}" type="text" id="codigo" class="form-control {{ $errors->has('codigo') ? 'is-invalid' : '' }}" name="codigo" placeholder="Ex: VR020">
                            <input value="{{ $produto->id }}" style="display:none"type="text"name="produto">
                            @else
                            <input type="text" id="codigo" class="form-control {{ $errors->has('codigo') ? 'is-invalid' : '' }}" name="codigo" placeholder="Ex: VR020">
                            @endif
                            <div class="invalid-feedback">
                                @if ($errors->has('codigo'))
                                    {{ $errors->first('codigo') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="form-group">
                            <label for="nome">Nome:</label> <label class="required" >*</label>
                            @if ($buscar == true)
                            <input readonly style="cursor: not-allowed;" value="{{ $produto->nome }}" type="text" id="nome" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome" placeholder="Ex: Regata">
                            @else
                            <input type="text" id="nome" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome" placeholder="Ex: Regata">
                            @endif
                            <div class="invalid-feedback">
                                @if ($errors->has('nome'))
                                    {{ $errors->first('nome') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="nome">Quantidade:</label> <label class="required" >*</label>
                            <input type="number" id="qtd" class="form-control {{ $errors->has('qtd') ? 'is-invalid' : '' }}" name="qtd" placeholder="Ex: 10">
                            <div class="invalid-feedback">
                                @if ($errors->has('qtd'))
                                    {{ $errors->first('qtd') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="tamanho">Tamanho:</label> <label class="required" >*</label>
                            <input type="text" id="tamanho" class="form-control {{ $errors->has('tamanho') ? 'is-invalid' : '' }}" name="tamanho" placeholder="Ex: 40">
                            <div class="invalid-feedback">
                                @if ($errors->has('tamanho'))
                                    {{ $errors->first('tamanho') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <label class="" for="escolhaLoja"> Loja: </label> <label class="required" >*</label>
                        <select class="custom-select form-control {{ $errors->has('escolhaLoja') ? 'is-invalid' : '' }}" name="escolhaLoja" id="escolhaLoja">
                            <option selected> Escolha... </option>
                            @foreach ($loja as $loja)
                                <option value="{{$loja->id}}"> {{$loja->nome}} </option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            @if ($errors->has('escolhaLoja'))
                                {{ $errors->first('escolhaLoja') }}
                            @endif
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="vlunitario">Valor Unitário:</label>
                            <div class="input-group mb-3">
                                <div style="cursor: not-allowed;" class="input-group-prepend">
                                  <span class="input-group-text">$</span>
                                </div>
                                @if ($buscar == true)
                                <input value="{{ $produto->valor_unitario }}" readonly style="cursor: not-allowed;" type="text" id="vlunitario" class="form-control {{ $errors->has('vlunitario') ? 'is-invalid' : '' }}"
                                aria-label="Amount (to the nearest dollar)" name="vlunitario" placeholder="99,99">
                                @else
                                <input readonly style="cursor: not-allowed;" type="text" id="vlunitario" class="form-control {{ $errors->has('vlunitario') ? 'is-invalid' : '' }}"
                                aria-label="Amount (to the nearest dollar)" name="vlunitario" placeholder="99,99">
                                @endif
                                <div class="invalid-feedback">
                                    @if ($errors->has('vlunitario'))
                                        {{ $errors->first('vlunitario') }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="grupo">Grupo:</label> <label class="required" >*</label>
                            @if ($buscar == true)
                            <input value="{{ $produto->grupo->nome }}" readonly style="cursor: not-allowed;" type="text" id="grupo" class="form-control {{ $errors->has('grupo') ? 'is-invalid' : '' }}" name="grupo" placeholder="Ex: 40">
                            @else
                            <input readonly style="cursor: not-allowed;" type="text" id="grupo" class="form-control {{ $errors->has('grupo') ? 'is-invalid' : '' }}" name="grupo" placeholder="Ex: 40">
                            @endif
                            <div class="invalid-feedback">
                                @if ($errors->has('grupo'))
                                    {{ $errors->first('grupo') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="colecao">Coleção:</label> <label class="required" >*</label>
                            @if ($buscar == true)
                            <input value="{{ $produto->colecao->nome }}" readonly style="cursor: not-allowed;" type="text" id="colecao" class="form-control {{ $errors->has('colecao') ? 'is-invalid' : '' }}" name="colecao" placeholder="Ex: 40">
                            @else
                            <input readonly style="cursor: not-allowed;" type="text" id="colecao" class="form-control {{ $errors->has('colecao') ? 'is-invalid' : '' }}" name="colecao" placeholder="Ex: 40">
                            @endif
                            <div class="invalid-feedback">
                                @if ($errors->has('colecao'))
                                    {{ $errors->first('colecao') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="pr-3" style="text-align: right;">
                    <button type="button" onClick="cancelar()" class="btn btn-cancel">Cancelar</button>
                    <button id="btnSalvar" type="submit" onkeydown="EvitaEnter(event);" class="btn btn-default ml-2">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('javascript3')

<script type="text/javascript">

    $('#codigo').keypress(function(event) {
        if (event.keyCode == '13') {
            buscarProduto()
            return false;
        }
    });

    function buscarProduto(){

        var codigo = document.getElementById('codigo').value
        var url = '{{ route("buscar_produto", ":codigo") }}';
        url = url.replace(':codigo', codigo);
        window.location.href = url;

    }

    function cancelar() {
        var url = '{{ route("lista_estoque") }}';
        window.location.href = url;
    }

</script>

@endsection
