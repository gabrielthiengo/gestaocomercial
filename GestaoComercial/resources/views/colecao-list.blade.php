@extends('layouts.app')

@section('body')

<div class="content">
        <div class="card border mt-3 table-style">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="card-title"><i class="fas fa-align-justify"></i>  Coleções </h5>
                    </div>
                    <div class="col-2" style="text-align: right;">
                        <a class="btn btn-create" href="{{ route('nova_colecao') }}"><i class="fas fa-plus"></i><strong> Coleção </strong></a>
                    </div>
                </div>
                <table class="table table-ordered table-hover table-striped" id="table_colecao">
                    <thead>
                        <tr>
                            <th>Código <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Descrição <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Deletar <i class="fas fa-sort-alpha-down ml-2"></i></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($colecoes as $colecao)
                            <tr>
                                <td>{{$colecao->codigo}}</td>
                                <td>{{$colecao->nome}}</td>
                                <td>{{$colecao->descricao}}</td>
                                <td> <a href="/colecoes/deletar/{{ $colecao->id }}" class="btn btn-sm btn-danger">Deletar</a> </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
</div>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {
    $('#table_colecao').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhuma coleção",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
