@extends('layouts.app')

@section('body')

    <form action="">
        @csrf
            <div class="venda">
                <div class="venda-search">
                <div class="jumbotron border border-secondary">
                    <div class="card-body body-card">
                        <h5 class="card-title"><i class="far fa-edit">   </i>   Selecionar Produto</h5>
                        <hr>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="nome">Código:</label> <label class="required" >*</label>
                                    <input onChange="buscarProduto()" type="text" id="nome" autocomplete="off" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome" placeholder="Ex: 12545">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('nome'))
                                            {{ $errors->first('nome') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="nome">Nome:</label> <label class="required" >*</label>
                                    <input type="text" id="nome" autocomplete="off" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome" placeholder="Ex: Regata">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('nome'))
                                            {{ $errors->first('nome') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="vendedor">Vendedor:</label> <label class="required" >*</label>
                                    <input type="text" id="vendedor" autocomplete="off" class="form-control {{ $errors->has('vendedor') ? 'is-invalid' : '' }}" name="vendedor" placeholder="Ex: Vendedor">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('vendedor'))
                                            {{ $errors->first('vendedor') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table" id="table_estoque">
                                <thead>
                                <tr>
                                    <th scope="col">Código <i class="fas fa-sort-alpha-down ml-2"></i></th>
                                    <th scope="col" style="min-width:100px !important; width:100px !important;">Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                                    <th scope="col" style="min-width:73px !important; width:75px !important;">Valor <i class="fas fa-sort-alpha-down ml-2"></i></th>
                                    <th scope="col">Estoque <i class="fas fa-sort-alpha-down ml-2"></i></th>
                                    <th scope="col" style="width:100px !important;">Qtd <i class="fas fa-sort-alpha-down ml-2"></i></th>
                                    <th style="text-align: right;" scope="col">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($estoque as $estoque)
                                    <tr>
                                        <th scope="row">{{$estoque->produto->codigo}}</th>
                                        <th style="font-weight: 100;">{{$estoque->produto->nome}}</th>
                                        <th style="font-weight: 100;">{{$estoque->produto->valor_unitario}}</th>
                                        <th style="font-weight: 100;">15</th>
                                        <th style="width: 45px;"><input type="number" id="nome" autocomplete="off" class="form-control" name="nome" placeholder="2"></th>
                                        <th style="text-align: right;"><a onclick="adicionarProduto()" href="#" class="btn btn-create" style="padding: 6px 12px;">Add</a></th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
                <div class="venda-detail">
                    <div class="cart-title2">
                        Carrinho
                    </div>
                    <div class="jumbotron border border-secondary mt-4">
                        <h5 class="card-title mt-3"style="margin-bottom: 6px !important;"><strong>Produtos:</strong></h5>
                        <table class="table">
                            <tbody>
                            <tr>
                                <button class="collapsible2">
                                    <div style="width:7%; float:left"><strong>5x</strong></div>
                                    <div style="width:53%; float:left"><strong>VR0154</strong> - Regata Básica</div>
                                    <div style="width:20%; float:left">R$ 349,50</div>
                                    <div style="width:20%; float:right; text-align: right; font-size: 16px"><i class="fas fa-angle-down"></i></div>
                                </button>
                                <div class="colapse2">
                                    <div class="colapse-content">
                                    <div style="float:left; width:50%; padding-bottom:10px !important; font-size: 14px;">
                                            Tamanho: <strong>42</strong>
                                            <br>
                                            Coleção: <strong>Inverno</strong>
                                            <br>
                                        </div>
                                        <div style="float:right; width:50%; padding-bottom:10px !important; font-size: 14px;">
                                            Valor Unitário: <strong>R$ 69,90</strong>
                                            <br>
                                            Grupo: <strong>Regatas</strong>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                            <tr>
                                <button class="collapsible2 mt-2">
                                    <div style="width:7%; float:left"><strong>2x</strong></div>
                                    <div style="width:53%; float:left"><strong>VR0154</strong> - Vestido Longo</div>
                                    <div style="width:20%; float:left">R$ 169,80</div>
                                    <div style="width:20%; float:right; text-align: right; font-size: 16px"><i class="fas fa-angle-down"></i></div>
                                </button>
                                <div class="colapse2">
                                    <div class="colapse-content">
                                        <div style="float:left; width:50%; padding-bottom:10px !important; font-size: 14px;">
                                            Tamanho: <strong>38</strong>
                                            <br>
                                            Coleção: <strong>Inverno</strong>
                                            <br>
                                        </div>
                                        <div style="float:right; width:50%; padding-bottom:10px !important; font-size: 14px;">
                                            Valor Unitário: <strong>R$ 84,90</strong>
                                            <br>
                                            Grupo: <strong>Vestidos</strong>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </tr>
                            </tbody>
                        </table>
                        <br>
                        <div class="row">
                            <div class="col-8">
                                <h5 class="card-title"style="margin-bottom: 6px !important; font-size: 15px;"><strong>Valor Total:</strong></h5>
                            </div>
                            <div class="col-4" style="text-align: right; padding-right: 12px;">
                                <h5 class="card-title"style="margin-bottom: 6px !important; font-size: 15px;"><strong>R$ 519,30</strong></h5>
                            </div>
                        </div>
                        <hr style="margin-top: 0px !important;">
                        <div class="mt-4" style="text-align: right;">
                            <button type="button" onClick="cancelar()" class="btn btn-cancel">Cancelar</button>
                            <button type="submit" class="btn btn-default ml-2">Confirmar</button>
                        </div>
                    </div>
                </div>
            </div>
    </form>

@endsection

@section('javascript2')

<script type="text/javascript">

    var coll = document.getElementsByClassName("collapsible2");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            event.preventDefault()
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
            content.style.maxHeight = null;
            } else {
            content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }

    function cancelar() {
        var url = '/';
        window.location.href = url;
    }

</script>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {
    $('#table_estoque').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhum vendedor",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
