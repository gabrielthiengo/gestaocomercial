@extends('layouts.app')

@section('body')

<div class="content">
    <div class="jumbotron border border-secondary">
        <div class="card-body">
            <h5 class="card-title mb-4">Cadastrar Grade</h5>
            <hr>
            <br>
            <form action="/grades" method="POST">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="codigo">Código:</label><label class="required" >*</label>
                            <input type="text" id="codigo" class="form-control {{ $errors->has('codigo') ? 'is-invalid' : '' }}" name="codigo" placeholder="Ex: 210015">
                            <div class="invalid-feedback">
                                @if ($errors->has('codigo'))
                                    {{ $errors->first('codigo') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="nome">Nome:</label><label class="required" >*</label>
                            <input type="text" id="nome" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome" placeholder="Ex: Grade X">
                            <div class="invalid-feedback">
                                @if ($errors->has('nome'))
                                    {{ $errors->first('nome') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t1">T1:</label>
                            <input type="text" id="t1" class="form-control {{ $errors->has('t1') ? 'is-invalid' : '' }}" name="t1" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t1'))
                                    {{ $errors->first('t1') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t2">T2:</label>
                            <input type="text" id="t2" class="form-control {{ $errors->has('t2') ? 'is-invalid' : '' }}" name="t2" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t2'))
                                    {{ $errors->first('t2') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t3">T3:</label>
                            <input type="text" id="t3" class="form-control {{ $errors->has('t3') ? 'is-invalid' : '' }}" name="t3" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t3'))
                                    {{ $errors->first('t3') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t4">T4:</label>
                            <input type="text" id="t4" class="form-control {{ $errors->has('t4') ? 'is-invalid' : '' }}" name="t4" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t4'))
                                    {{ $errors->first('t4') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t5">T5:</label>
                            <input type="text" id="t5" class="form-control {{ $errors->has('t5') ? 'is-invalid' : '' }}" name="t5" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t5'))
                                    {{ $errors->first('t5') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t6">T6:</label>
                            <input type="text" id="t6" class="form-control {{ $errors->has('t6') ? 'is-invalid' : '' }}" name="t6" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t6'))
                                    {{ $errors->first('t6') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t7">T7:</label>
                            <input type="text" id="t7" class="form-control {{ $errors->has('t7') ? 'is-invalid' : '' }}" name="t7" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t7'))
                                    {{ $errors->first('t7') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t8">T8:</label>
                            <input type="text" id="t8" class="form-control {{ $errors->has('t8') ? 'is-invalid' : '' }}" name="t8" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t8'))
                                    {{ $errors->first('t8') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t9">T9:</label>
                            <input type="text" id="t9" class="form-control {{ $errors->has('t9') ? 'is-invalid' : '' }}" name="t9" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t9'))
                                    {{ $errors->first('t9') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-group">
                            <label for="t0">T0:</label>
                            <input type="text" id="t0" class="form-control {{ $errors->has('t0') ? 'is-invalid' : '' }}" name="t0" >
                            <div class="invalid-feedback">
                                @if ($errors->has('t0'))
                                    {{ $errors->first('t0') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pr-3" style="text-align: right;">
                    <button type="button" onClick="cancelar()" class="btn btn-cancel">Cancelar</button>
                    <button type="submit" class="btn btn-default ml-2">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('javascript')

<script type="text/javascript">

  function cancelar() {
        var url = '{{ route("grades-list") }}';
        window.location.href = url;
    }

</script>

@endsection
