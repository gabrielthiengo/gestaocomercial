@extends('layouts.app')

@section('body')

    <div class="content">
        <div class="card border mt-3 table-style">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="card-title"><i class="fas fa-align-justify"></i>  Grupos </h5>
                    </div>
                    <div class="col-2" style="text-align: right;">
                        <a class="btn btn-create" href="{{ route('novo_grupo') }}"><i class="fas fa-plus"></i><strong> Grupo </strong></a>
                    </div>
                </div>
                <table class="table table-ordered table-hover table-striped" id="table_grupo">
                    <thead>
                        <tr>
                            <th>Código <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Descrição <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Deletar</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($grupos as $grupo)
                            <tr>
                                <td>{{$grupo->codigo}}</td>
                                <td>{{$grupo->nome}}</td>
                                <td>{{$grupo->descricao}}</td>
                                <td> <a href="/grupos/deletar/{{ $grupo->id }}" class="btn btn-sm btn-danger">Deletar</a> </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {
    $('#table_grupo').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhum grupo",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
