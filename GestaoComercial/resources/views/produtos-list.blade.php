@extends('layouts.app')

@section('body')

<div class="content">
        <div class="card border mt-3 table-style">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="card-title"><i class="fas fa-archive"></i> Lista de Produtos </h5>
                    </div>
                    <div class="col-2" style="text-align: right;">
                        <a class="btn btn-create" href="{{ route('novo_produto') }}"><i class="fas fa-plus"></i><strong> Produtos </strong></a>
                    </div>
                </div>
                <table class="table table-ordered table-hover table-striped" id="table_produto">
                    <thead>
                        <tr>
                            <th>Código <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Valor Unitário <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Valor Revenda <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Deletar</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($produtos as $produto)
                            <tr>
                                <td>{{$produto->codigo}}</td>
                                <td>{{$produto->nome}}</td>
                                <td class="currency">{{$produto->valor_unitario}}</td>
                                <td class="currency">{{$produto->valor_revenda}}</td>
                                <td> <a href="/produtos/deletar/{{ $produto->id }}" class="btn btn-sm btn-danger">Deletar</a> </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
</div>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {

    //$('.currency').mask('00.000,0', {reverse: true});

    $('#table_produto').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhum produto",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
