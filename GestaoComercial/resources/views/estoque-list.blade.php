@extends('layouts.app')

@section('body')

<div class="content">
        <div class="card border mt-3 table-style">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="card-title"><i class="fas fa-folder-open"></i>   Estoque</h5>
                    </div>
                    <div class="col-2" style="text-align: right;">
                        <a class="btn btn-create" href="{{ route('add_produto') }}"><i class="fas fa-plus"></i><strong>   Produto</strong></a>
                    </div>
                </div>

                <div class="infos">
                    <div class="info-left">
                        <div class="info-text">
                            Quantidade total: {{$count}}
                            <br>
                            <div style="width: auto; float: left;">
                                Valor total: R$
                            </div>
                            <div style="width: auto; float: left; margin-left: 2px;" class="currency">
                                {{$sum}}
                            </div>
                        </div>
                    </div>
                </div>


                <table class="table table-ordered table-hover table-striped" id="table_estoque">
                    <thead>
                        <tr>
                            <th>Código <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th style="width: 120px !important;">Qtde <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Tamanho <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Valor Unitário <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Valor Total <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Coleção <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Loja <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Cód. Barras <i class="fas fa-sort-alpha-down ml-2"></i></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($estoques as $estoque)
                            <tr>
                                <th>{{$estoque->produto->codigo}}</th>
                                <th style="font-weight: 100;">{{$estoque->produto->nome}}</th>
                                <th style="font-weight: 100;">{{$estoque->quantidade}}</th>
                                <th style="font-weight: 100;">{{$estoque->tamanho}}</th>
                                <th class="currency" style="font-weight: 100;">{{$estoque->produto->valor_unitario}}</th>
                                <th class="currency" style="font-weight: 100;">{{$estoque->valor_total}}</th>
                                <th style="font-weight: 100;">{{$estoque->produto->colecao->nome}}</th>
                                <th style="font-weight: 100;">{{$estoque->loja->nome}}</th>
                                <th style="font-weight: 100; font-size: 12px; letter-spacing: 1px;">
                                    <div style="text-align: center;">
                                        <img style="width: 100px; height: 20px;" src="data:image/png;base64,{{ base64_encode($estoque->barcode) }} " alt="">
                                    </div>
                                    <div style="text-align: center;">
                                        {{$estoque->barcode_string}}
                                    </div>
                                </th>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
</div>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {

    //$('.currency').mask('00.000,0', {reverse: true});

    $('#table_estoque').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhum produto",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
