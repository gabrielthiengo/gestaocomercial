@extends('layouts.app')

@section('body')

<div class="content">
        <div class="card border mt-3 table-style">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="card-title"><i class="fas fa-align-justify"></i>   Lista de Grades</h5>
                    </div>
                    <div class="col-2" style="text-align: right;">
                        <a class="btn btn-create" href="{{ route('nova_grade') }}"><i class="fas fa-plus"></i><strong>   Grade</strong></a>
                    </div>
                </div>
                <table class="table table-ordered table-hover table-striped" id="table_grade">
                    <thead>
                        <tr>
                            <th>Código <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T1 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T2 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T3 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T4 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T5 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T6 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T7 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T8 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T9 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>T0 <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Deletar</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($grades as $grade)
                            <tr>
                                <td>{{$grade->codigo}}</td>
                                <td>{{$grade->nome}}</td>
                                <td>{{$grade->T1}}</td>
                                <td>{{$grade->T2}}</td>
                                <td>{{$grade->T3}}</td>
                                <td>{{$grade->T4}}</td>
                                <td>{{$grade->T5}}</td>
                                <td>{{$grade->T6}}</td>
                                <td>{{$grade->T7}}</td>
                                <td>{{$grade->T8}}</td>
                                <td>{{$grade->T9}}</td>
                                <td>{{$grade->T0}}</td>
                                <td> <a href="/grades/deletar/{{ $grade->id }}" class="btn btn-sm btn-danger">Deletar</a> </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
</div>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {
    $('#table_grade').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhuma grade",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
