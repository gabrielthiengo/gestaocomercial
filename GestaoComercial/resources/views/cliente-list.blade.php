@extends('layouts.app')

@section('body')

<div class="content">
        <div class="card border mt-3 table-style">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h5 class="card-title"><i class="fas fa-user-circle"></i> Lista de Clientes </h5>
                    </div>
                    <div class="col-2" style="text-align: right;">
                        <a class="btn btn-create" href="{{ route('novo_cliente') }}"><i class="fas fa-plus"></i><strong> Cliente </strong></a>
                    </div>
                </div>
                <table class="table table-ordered table-hover table-striped" id="table_cliente">
                    <thead>
                        <tr>
                            <th>Id <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Nome <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Telefone Fixo <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Celular <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Email <i class="fas fa-sort-alpha-down ml-2"></i></th>
                            <th>Deletar <i class="fas fa-sort-alpha-down ml-2"></i></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($clientes as $cliente)
                            <tr>
                                <td>{{$cliente->id}}</td>
                                <td>{{$cliente->nome}}</td>
                                <td>{{$cliente->telefone_fixo}}</td>
                                <td>{{$cliente->telefone_celular}}</td>
                                <td>{{$cliente->email}}</td>
                                <td> <a href="/clientes/deletar/{{ $cliente->id }}" class="btn btn-sm btn-danger">Deletar</a> </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
</div>

@endsection

@section('javascriptPagination')

<script type="text/javascript">

$(document).ready(function() {
    $('#table_cliente').DataTable( {
        "language": {
            "search": "Filtrar",
            "lengthMenu": "Itens por página _MENU_",
            "zeroRecords": "Nenhum cliente",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum Registro",
            "infoFiltered": "",
            "paginate": {
                "previous": "Início",
                "next": "Próxima",
                "last": "Última",
            }
        }
    } );
} );

</script>

@endsection
