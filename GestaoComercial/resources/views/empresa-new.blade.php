@extends('layouts.app')

@section('body')

<div class="content">
    <div class="jumbotron border border-secondary">
        <div class="card-body">
            <h5 class="card-title mb-4">Cadastrar Empresa</h5>
            <hr>
            <br>
            <form action="/empresas" method="POST">
                @csrf

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="nome">Nome Fantasia:</label>
                            <input type="text" id="nome" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" name="nome">
                            <div class="invalid-feedback">
                                @if ($errors->has('nome'))
                                    {{ $errors->first('nome') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="cnpj"> CNPJ :</label>
                            <input type="text" id="cnpj" class="form-control {{ $errors->has('cnpj') ? 'is-invalid' : '' }}" name="cnpj">
                            <div class="invalid-feedback">
                                @if ($errors->has('cnpj'))
                                    {{ $errors->first('cnpj') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="razaoSocial"> Razão Social:</label>
                            <input type="text" id="razaoSocial" class="form-control {{ $errors->has('razaoSocial') ? 'is-invalid' : '' }}" name="razaoSocial">
                            <div class="invalid-feedback">
                                @if ($errors->has('razaoSocial'))
                                    {{ $errors->first('razaoSocial') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="inscEstadual"> Inscrição Estadual:</label>
                            <input type="text" id="inscEstadual" class="form-control {{ $errors->has('inscEstadual') ? 'is-invalid' : '' }}" name="inscEstadual">
                            <div class="invalid-feedback">
                                @if ($errors->has('inscEstadual'))
                                    {{ $errors->first('inscEstadual') }}
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="inscMunicipal"> Inscrição Municipal:</label>
                            <input type="text" id="inscMunicipal" class="form-control {{ $errors->has('inscMunicipal') ? 'is-invalid' : '' }}" name="inscMunicipal">
                            <div class="invalid-feedback">
                                @if ($errors->has('inscMunicipal'))
                                    {{ $errors->first('inscMunicipal') }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <hr>

                <div class="pr-3" style="text-align: right;">
                    <button type="cancel" class="btn btn-cancel">Cancelar</button>
                    <button type="submit" class="btn btn-default ml-2">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
