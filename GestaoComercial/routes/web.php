<?php

use App\Http\Controllers\ClienteController;


Route::get('/', function () {

    return view('index');
});

// Cliente - Get
Route::get('/clientes', 'ClienteController@index')->name('lista_clientes');
Route::get('/clientes/novo', 'ClienteController@create')->name('novo_cliente');
Route::get('/clientes/deletar/{id}', 'ClienteController@destroy');
// Cliente - POST
Route::post('/clientes', 'ClienteController@store');

//Vendedor Get
Route::get('/vendedor', 'VendedorController@index')->name('lista_vendedores');
Route::get('/vendedor/novo', 'VendedorController@create')->name('novo_vendedor');
Route::get('/vendedor/deletar/{id}', 'VendedorController@destroy');
//Vendedor POST
Route::post('/vendedor-create', 'VendedorController@store');

//Empresa Get
Route::get('/empresas/novo', 'EmpresaController@create')->name('nova_empresa');
//Empresa POST
Route::post('/empresas', 'EmpresaController@store');

//Grades Get
Route::get('/grades', 'GradesController@index')->name('grades-list');
Route::get('/grades/novo', 'GradesController@create')->name('nova_grade');
Route::get('/grades/deletar/{id}', 'GradesController@destroy');
//Grades POST
Route::post('/grades', 'GradesController@store');

//Grupo Get
Route::get('/grupos','GrupoController@index')->name('grupos_list');
Route::get('/grupos/novo','GrupoController@create')->name('novo_grupo');
Route::get('/grupos/deletar/{id}', 'GrupoController@destroy');
//Grupo POST
Route::post('/grupos', 'GrupoController@store');

//Coleção Get
Route::get('/colecoes','ColecaoController@index')->name('colecoes_list');
Route::get('/colecoes/novo','ColecaoController@create')->name('nova_colecao');
Route::get('/colecoes/deletar/{id}', 'ColecaoController@destroy');
//Coleção POST
Route::post('/colecoes', 'ColecaoController@store');

//Vendas Get
Route::get('/venda/novo','VendaXProdutoController@create')->name('nova_venda');
//Vendas POST

//Produtos Get
Route::get('/produtos', 'ProdutoController@index')->name('lista_produtos');
Route::get('/produtos/novo','ProdutoController@create')->name('novo_produto');
Route::get('/produtos/deletar/{id}', 'ProdutoController@destroy');
//Produtos POST
Route::post('/produtos', 'ProdutoController@store');

//Estoque GET
Route::get('/estoque', 'EstoqueController@index')->name('lista_estoque');
Route::get('/estoque/produto','EstoqueController@create')->name('add_produto');
Route::get('/estoque/produto/{codigo}','EstoqueController@show')->name('buscar_produto');
//Estoque POST
Route::post('/estoque', 'EstoqueController@store');
